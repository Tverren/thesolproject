#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <scene/gmsceneobject>
#include "sphere.h"
#include "collision_object.h"
#include <vector>
#include <stdlib.h>


template<typename T>
class Controller : public GMlib::SceneObject{
    GM_SCENEOBJECT(Controller)

    public:
        Controller(){
        this->setSurroundingSphere(GMlib::Sphere<float,3>(100.0f));

        sizeScaling = 30000;

        steps = 0;

/*
        earthSize = 800;
        earthMass = 5.97237e24*universal_scaling;
        earthDensity = 300;
        earthVelocity = 29780.0f*universal_scaling;
        earthPosition = 1.50e11*universal_scaling;


        moonSize = earthSize*0.273;
        moonMass = 7.342e22*universal_scaling;
        moonDensity = 300;
        moonVelocity = 1022.0f*universal_scaling;
        moonPosition = 384399000*universal_scaling;



        sunSize = 400*2;
        sunMass = 1.98855e30*universal_scaling;
        sunDensity = 300;
        sunPosition = 0.0f;

        mercurySize = earthSize*0.3829f;
        mercuryMass = 3.3011e23*universal_scaling;
        //mercuryMass = earthMass*0.055;
        mercuryDensity = 300;
        mercuryVelocity = 45362.0f*universal_scaling;
        mercuryPosition = 5.79e10*universal_scaling;

        venusSize = earthSize*0.9499f;
        venusMass = 4.8675e24*universal_scaling;
        //venusMass = earthMass;
        venusDensity = 300;
        venusVelocity = 35020.0f*universal_scaling;
        venusPosition = 1.08e11*universal_scaling;


        marsSize = earthSize*0.533f;
        marsMass = 6.4171e23*universal_scaling;
        //marsMass = earthMass;
        marsDensity = 300;
        marsVelocity = 24077.0f*universal_scaling;
        marsPosition = 2.28e11*universal_scaling;


        jupiterSize = earthSize*11.209f;
        jupiterMass = 1.8986e27*universal_scaling;
        //jupiterMass = earthMass;
        jupiterDensity = 300;
        jupiterVelocity = 13070.0f*universal_scaling;
        jupiterPosition = 7.78e11*universal_scaling;


        saturnSize = earthSize*9.4492f;
        saturnMass = 5.6836e26*universal_scaling;
        //saturnMass = earthMass;
        saturnDensity = 300;
        saturnVelocity = 9690.0f*universal_scaling;
        saturnPosition = 1.43e12*universal_scaling;


        uranusSize = earthSize*4.007f;
        uranusMass = 8.6810e25*universal_scaling;
        //uranusMass = earthMass;
        uranusDensity = 300;
        uranusVelocity = 6800.0f*universal_scaling;
        uranusPosition = 2.87e12*universal_scaling;


        neptuneSize = earthSize*3.883f;
        neptuneMass = 1.0243e26*universal_scaling;
        //neptuneMass = earthMass;
        neptuneDensity = 300;
        neptuneVelocity = 5430.0f*universal_scaling;
        neptunePosition = 4.50e12*universal_scaling;
*/

        //addSphere();
        //addTwoSphereCollHor();
        //addTwoSphereCollVer();
        //addTwoSphereOrb();
        addThreeSphereOrb();

    }
    void addSphere();
    void addTwoSphereCollHor();
    void addTwoSphereCollVer();
    void addTwoSphereOrb();
    void addThreeSphereOrb();

    ~Controller();


private:
    //constexpr static double universal_scaling = 1e-6;
    //constexpr static double universal_gravity = 6.674e-11*universal_scaling;
    constexpr static double universal_gravity = 6.674e-11;

    //T dtPrev = 0.0f;
    //T dtPrevPrev = 0.0f;

    T sizeScaling;

    T sunSize;
    T sunMass;
    T sunDensity;
    T sunPosition;

    T venusSize;
    T venusMass;
    T venusDensity;
    T venusPosition;
    T venusVelocity;

    T mercurySize;
    T mercuryMass;
    T mercuryDensity;
    T mercuryPosition;
    T mercuryVelocity;

    T earthSize;
    T earthMass;
    T earthDensity;
    T earthPosition;
    T earthVelocity;

    T moonSize;
    T moonMass;
    T moonDensity;
    T moonVelocity;
    T moonPosition;

    T marsSize;
    T marsMass;
    T marsDensity;
    T marsPosition;
    T marsVelocity;

    T jupiterSize;
    T jupiterMass;
    T jupiterDensity;
    T jupiterPosition;
    T jupiterVelocity;

    T saturnSize;
    T saturnMass;
    T saturnDensity;
    T saturnPosition;
    T saturnVelocity;

    T uranusSize;
    T uranusMass;
    T uranusDensity;
    T uranusPosition;
    T uranusVelocity;

    T neptuneSize;
    T neptuneMass;
    T neptuneDensity;
    T neptunePosition;
    T neptuneVelocity;



    int steps;




    GMlib::Vector<T,3> compute_force(std::shared_ptr<Sphere<T>> planet1,
                                     std::shared_ptr<Sphere<T>> planet2);

    std::vector<std::shared_ptr<Sphere<T>>> planetOfSphere;//array

    bool find_collision(std::shared_ptr<Sphere<T>> pi ,std::shared_ptr<Sphere<T>> pj,
                        T dt, T x, Collision_object<T> &object);

    void collision_detection(T dt);
    void make_force(T dt);

    void compute_force(T dt);

protected:
    void localSimulate(double dt) {

        if(steps < 20){
            steps++;
        }

        T ndt = steps*dt;

        //make_force(dt);

        //std::cout << "dt  " << dt<< std::endl;

        compute_force(ndt);
        collision_detection(ndt);
    }
};

#include "controller.c"

#endif // CONTROLLER_H

