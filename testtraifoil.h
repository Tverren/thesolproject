#ifndef TESTTRAIFOIL_H
#define TESTTRAIFOIL_H



#include <parametrics/gmptrianguloidtrefoil>


class TestTraiFoil : public GMlib::PTrianguloidTrefoil<float> {
public:
  using PTrianguloidTrefoil::PTrianguloidTrefoil;

  ~TestTraiFoil() {

    if(m_test01)
      remove(test_01_trifoil.get());
    if(m_test02)
      remove(test_02_trifoil.get());
    if(m_test03)
      remove(test_03_trifoil.get());
    if(m_test04)
      remove(test_04_trifoil.get());
    }


  void test01() {

    GMlib::Vector<float,3> d1 = evaluate(0.0f,0.0f,0,0)[0][0];
    test_01_trifoil = std::make_shared<TestTraiFoil>();
    test_01_trifoil->translate(d1 + d1.getNormalized()*20.0f);
    test_01_trifoil->rotate( GMlib::Angle(0), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f) );
    test_01_trifoil->toggleDefaultVisualizer();
    test_01_trifoil->setMaterial(GMlib::GMmaterial::Obsidian);
    test_01_trifoil->replot(200,200,1,1);
    insert(test_01_trifoil.get());


    test_02_trifoil = std::make_shared<TestTraiFoil>();
    test_02_trifoil->translate(d1 - d1.getNormalized()*20.0f);
    test_02_trifoil->rotate( GMlib::Angle(90), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f) );
    test_02_trifoil->toggleDefaultVisualizer();
    test_02_trifoil->setMaterial(GMlib::GMmaterial::Ruby);
    test_02_trifoil->replot(200,200,1,1);
    insert(test_02_trifoil.get());


    GMlib::Vector<float,3> d2 = evaluate(0.0f,0.0f,0,0)[1][0];
    test_03_trifoil = std::make_shared<TestTraiFoil>();
    test_03_trifoil->translate(d2 + d2.getNormalized()*20.0f);
    test_03_trifoil->rotate( GMlib::Angle(270), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f) );
    test_03_trifoil->toggleDefaultVisualizer();
    test_03_trifoil->setMaterial(GMlib::GMmaterial::Emerald);
    test_03_trifoil->replot(200,200,1,1);
    insert(test_03_trifoil.get());


    test_04_trifoil = std::make_shared<TestTraiFoil>();
    test_04_trifoil->translate(d2 - d2.getNormalized()*30.0f);
    test_04_trifoil->rotate( GMlib::Angle(360), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f) );
    test_04_trifoil->toggleDefaultVisualizer();
    test_04_trifoil->setMaterial(GMlib::GMmaterial::Sapphire);
    test_04_trifoil->replot(200,200,1,1);
    insert(test_04_trifoil.get());

    //test_01_trifoil->insert(test_04_trifoil.get());

    //test_01_trifoil->lock(test_02_trifoil.get());
    //test_03_trifoil->lock(test_04_trifoil.get());
    //test_03_trifoil->lock(test_01_trifoil.get());
    //test_02_trifoil->lock(test_01_trifoil.get());

    m_test01 = true;
    m_test02 = true;
    m_test03 = true;
    m_test04 = true;
  }


protected:
  void localSimulate(double dt) override {


    rotate( GMlib::Angle(90) * dt, GMlib::Vector<float,3>( 0.0f, 1.0f, 0.0f ) );
    //rotate( GMlib::Angle(180) * dt, GMlib::Vector<float,3>( 1.0f, 1.0f, 1.0f ) );
  }

private:
  bool m_test01 {false};
  bool m_test02 {false};
  bool m_test03 {false};
  bool m_test04 {false};
  std::shared_ptr<TestTraiFoil> test_01_trifoil {nullptr};
  std::shared_ptr<TestTraiFoil> test_02_trifoil {nullptr};
  std::shared_ptr<TestTraiFoil> test_03_trifoil {nullptr};
  std::shared_ptr<TestTraiFoil> test_04_trifoil {nullptr};

}; // END class TestTorus



#endif // TESTTORUS_H
