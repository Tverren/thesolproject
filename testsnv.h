
/* \file TestSurfaceNineVector.h
 *
 *  Interface for the TestSurfaceNineVector class.
 */

#ifndef __TestSurfaceNineVector_H__
#define __TestSurfaceNineVector_H__

#include "../gmlib/modules/parametrics/src/gmpsurf.h"


namespace GMlib {

template <typename T>
class TestSurfaceNineVector : public PSurf<T,3> {
    GM_SCENEOBJECT(TestSurfaceNineVector)
    public:
        TestSurfaceNineVector(  const DMatrix<Vector<T,3>>& c );
    TestSurfaceNineVector( const TestSurfaceNineVector<T>& copy );
    virtual ~TestSurfaceNineVector();

protected:
    DMatrix<Vector<T,3>>		            _c;

    void                      eval(T u, T v, int d1, int d2, bool lu = true, bool lv = true );
    T                         getEndPU();
    T                         getEndPV();
    T                         getStartPU();
    T                         getStartPV();

}; // END class TestSurfaceNineVector

} // END namespace GMlib

// Include TestSurfaceNineVector class function implementations
#include "testsnv.c"



#endif // __TestSurfaceNineVector_H__
