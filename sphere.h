#ifndef SPHERE_H
#define SPHERE_H

#include <parametrics/gmpsphere>

template<typename T>
class Sphere : public GMlib::PSphere<float> {
    GM_SCENEOBJECT(Sphere)
    public:

        T getSize(){return size;}
    T getMass(){return mass;}
    T getDensity(){return density;}
    GMlib::Vector<T,3> getStep(){return stepCurrent;}
    GMlib::Vector<T,3> getVelocity(){return velocity;}
    GMlib::Vector<T,3> getForce(){return force;}
    GMlib::Vector<T,3> getForcePrevious(){return forcePrevious;}
    GMlib::Vector<T,3> getForcePreviousPrevious(){return forcePreviousPrevious;}
    GMlib::Vector<T,3> getForcePreviousPreviousPrevious(){return forcePreviousPreviousPrevious;}


    Sphere(T size, T mass, T density,
           GMlib::Vector<T,3> velocity):GMlib::PSphere<float>(float(size)){
        this->mass = mass;
        //std::cout << "mass                      " <<mass<< std::endl;
        this->density = density;
        this->velocity = velocity;
        this->force = GMlib::Vector<T,3>(0.0f,0.0f,0.0f);
        this->forcePrevious = GMlib::Vector<T,3>(0.0f,0.0f,0.0f);
        this->forcePreviousPrevious = GMlib::Vector<T,3>(0.0f,0.0f,0.0f);
        this->forcePreviousPreviousPrevious = GMlib::Vector<T,3>(0.0f,0.0f,0.0f);


        accArray[1][0] = accArray[0][0] = T(0);
        accArray[1][1] = accArray[0][1] = T(0);
        accArray[1][2] = accArray[0][2] = T(0);
        accArray[1][3] = accArray[0][3] = T(0);


        //std::cout << "getMass()                      " <<getMass()<< std::endl;
    }


    void update_force(GMlib::Vector<T,3> forceNew){
        this->force += forceNew;

        //std::cout << "force                      " <<force.getLength()<< std::endl;
    }

    void update_velocity(GMlib::Vector<T,3> velocityNew){
        this->velocity += velocityNew;

        //std::cout << " "<< std::endl;
        //std::cout << "velocity                      " <<velocity.getLength()<< std::endl;
    }

    void reset_force(){
        this->force = GMlib::Vector<T,3>(0.0f,0.0f,0.0f);
    }

    void update_step(GMlib::Vector<T,3> stepNew){
        this->stepCurrent = stepNew;

        //std::cout << "stepCurrent                      " <<stepCurrent.getLength()<< std::endl;
    }

    void update_sphere(GMlib::Vector<T,3> newVelocity){
        //this->stepCurrent = -stepCurrent;
        this->velocity = newVelocity;
    }


    void compute_step(T dt){


        //std::cout << "velocity " << velocity.getLength() << std::endl;

        //std::cout << "force " << force.getLength() << std::endl;

        //std::cout << "mass " << mass << std::endl;

        //std::cout << "dt " << dt << std::endl;



        accArray[1][0] = accArray[0][0];
        accArray[1][1] = accArray[0][1];
        accArray[1][2] = accArray[0][2];
        accArray[1][3] = accArray[0][3];


        accArray[0][0] = force/mass;
        accArray[0][1] = (accArray[1][0]-accArray[0][0])/dt;
        accArray[0][2] = (accArray[1][1]-accArray[0][1])/dt;
        accArray[0][3] = (accArray[1][2]-accArray[0][2])/dt;




        //GMlib::Vector<T,3> force_derived =        (forcePrevious - force)/dt;
        //GMlib::Vector<T,3> force_derived_double = (forcePreviousPrevious - force)/(dt+dtPrev);
        //GMlib::Vector<T,3> force_derived_triple = (forcePreviousPreviousPrevious - force)/(dt+dtPrevPrev+dtPrev);


        auto ds =        dt*velocity
                + (1/2*  dt*dt)*         accArray[0][0]
                + (1/6*  dt*dt*dt)*      accArray[0][1]
                + (1/24* dt*dt*dt*dt)*   accArray[0][2]
                + (1/120*dt*dt*dt*dt*dt)*accArray[0][3];

        //std::cout << "ds " << ds << std::endl;

        update_velocity  (dt*          accArray[0][0]
                  + (1/2* dt*dt)*      accArray[0][1]
                  + (1/6* dt*dt*dt)*   accArray[0][2]
                  + (1/24*dt*dt*dt*dt)*accArray[0][3]);

        update_step(ds);
        reset_force();

        //dtPrevPrev = dtPrev;
        //dtPrev = dt;
    }




protected:
    void localSimulate(double dt) {

        //this->rotate(GMlib::Angle(90)*dt,GMlib::Vector<float,3>(0.0f, 0.0f, 1.0f));


        GMlib::Vector<float,3> stepVector(stepCurrent[0],stepCurrent[1],stepCurrent[2]);


        //std::cout << "stepVectorFloat " << stepVectorFloat << std::endl;

        this->translateGlobal(stepVector);
    }

private:
    T size;
    T mass;
    T density;
    T dtPrev = 0.0f;
    T dtPrevPrev = 0.0f;
    GMlib::Vector<T,3> velocity;
    GMlib::Vector<T,3> force;
    GMlib::Vector<T,3> forcePrevious;
    GMlib::Vector<T,3> forcePreviousPrevious;
    GMlib::Vector<T,3> forcePreviousPreviousPrevious;
    GMlib::Vector<T,3> stepCurrent;

    GMlib::Vector<GMlib::Vector<GMlib::Vector<T,3>, 2>,4> accArray;

}; // END class Sphere

#endif // SPHERE_H
