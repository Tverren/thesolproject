#ifndef TESTSPHERE_H
#define TESTSPHERE_H

#include <parametrics/gmpsphere>


class TestTESTYSphere : public GMlib::PSphere<float> {
public:
  using PSphere::PSphere;

  ~TestTESTYSphere() {

    if(m_test01)
      remove(test_01_tesphere.get());
    }


  void test01() {

    GMlib::Vector<float,3> d1 = evaluate(0.0f,0.0f,0,0)[0][0];
    test_01_tesphere = std::make_shared<TestTESTYSphere,float>(1.0f);
    test_01_tesphere->translate(d1 + d1.getNormalized()*20.0f);
    test_01_tesphere->rotate( GMlib::Angle(0), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f) );
    test_01_tesphere->toggleDefaultVisualizer();
    test_01_tesphere->setMaterial(GMlib::GMmaterial::Chrome);
    test_01_tesphere->replot(200,200,1,1);
    insert(test_01_tesphere.get());

    m_test01 = true;
  }

protected:
  void localSimulate(double dt) override {

      rotate( GMlib::Angle(180) * dt, GMlib::Vector<float,3>( 1.0f, 0.0f, 1.0f ) );
      //rotate( GMlib::Angle(180) * dt, GMlib::Vector<float,3>( 1.0f, 1.0f, 0.0f ) );
  }

private:
  bool m_test01 {false};
  std::shared_ptr<TestTESTYSphere> test_01_tesphere {nullptr};

}; // END class TestTorus

#endif // TESTSPHERE_H
