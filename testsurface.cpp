/* \file testsurface.cpp
 *
 *  Implementation of the testSurface template class.
 */


// for syntaxhighlighting
#include "testsurface.h"

namespace GMlib {


template <typename T>
inline
TestSurface<T>::TestSurface( const Point<T,3>& p1,
                             const Point<T,3>& p2,
                             const Point<T,3>& p3,
                             const Point<T,3>& p4 ) {

    _pt1  = p1;
    _pt2  = p2;
    _pt3  = p3;
    _pt4  = p4;

    this->_dm = GM_DERIVATION_EXPLICIT;
}

template <typename T>
inline
TestSurface<T>::TestSurface( const TestSurface<T>& copy ) : PSurf<T,3>( copy ) {

    _pt1  = copy._pt1;
    _pt2  = copy._pt2;
    _pt3  = copy._pt3;
    _pt4  = copy._pt4;
}


template <typename T>
TestSurface<T>::~TestSurface() {}

template <typename T>
void TestSurface<T>::eval(T u, T v, int d1, int d2, bool /*lu*/, bool /*lv*/ ) {

    this->_p.setDim( d1+1, d2+1 );

    this->_p[0][0] = _pt1+u*(_pt2-_pt1)+v*(_pt4-_pt1+u*(_pt1-_pt2+_pt3-_pt4));

    if( this->_dm == GM_DERIVATION_EXPLICIT ) {

        // 1st
        if(d1)            this->_p[1][0] = _pt2-_pt1+v*(_pt1-_pt2+_pt3-_pt4); // S_u
        if(d2)            this->_p[0][1] = _pt4-_pt1+u*(_pt1-_pt2+_pt3-_pt4); // S_v
        if(d1>1 && d2>1)  this->_p[1][1] = _pt1-_pt2+_pt3-_pt4; // S_uv

        // 2nd
        if(d1>1)          this->_p[2][0] = Vector<T,3>(T(0)); // S_uu
        if(d2>1)          this->_p[0][2] = Vector<T,3>(T(0)); // S_vv
        if(d1>1 && d2)    this->_p[2][1] = Vector<T,3>(T(0)); // S_uuv
        if(d1   && d2>1)  this->_p[1][2] = Vector<T,3>(T(0)); // S_uvv
        if(d1>1 && d2>1)  this->_p[2][2] = Vector<T,3>(T(0)); // S_uuvv

        // 3rd
        if(d1>2)          this->_p[3][0] = Vector<T,3>(T(0)); // S_uuu
        if(d2>2)          this->_p[0][3] = Vector<T,3>(T(0)); // S_vvv
        if(d1>2 && d2)    this->_p[3][1] = Vector<T,3>(T(0)); // S_uuuv
        if(d1   && d2>2)  this->_p[1][3] = Vector<T,3>(T(0)); // S_uvvv
        if(d1>2 && d2>1)  this->_p[3][2] = Vector<T,3>(T(0)); // S_uuuvv
        if(d1>1 && d2>2)  this->_p[2][3] = Vector<T,3>(T(0)); // S_uuvvv
        if(d1>2 && d2>2)  this->_p[3][3] = Vector<T,3>(T(0)); // S_uuuvvv
    }
}


template <typename T>
inline
T TestSurface<T>::getEndPU()	{

    return T(1);
}

template <typename T>
inline
T TestSurface<T>::getEndPV()	{

    return T(1);
}


template <typename T>
inline
T TestSurface<T>::getStartPU() {

    return T(0);
}


template <typename T>
inline
T TestSurface<T>::getStartPV() {

    return T(0);
}
}
