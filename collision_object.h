#ifndef COLLISION_OBJECT
#define COLLISION_OBJECT
#include "sphere.h"

template<typename T>
class Collision_object{

public:

    std::shared_ptr<Sphere<T>> getObj(int i) const{
        if(i == 1)
            return planet1;
        if(i == 2)
            return planet2;
        else
            return nullptr;
    }

    void setObj(int i, std::shared_ptr<Sphere<T>> newPlanet){
        if(i == 1)
            this->planet1 = newPlanet;
        if(i == 2)
            this->planet2 = newPlanet;
    }

    T getX() const{
        return x;
    }

    void setX(T newX){
        this->x = newX;
    }

    const operator <(const Collision_object<T> &comparingObjects){
        return x < comparingObjects.getX();
    }

    const operator ==(const Collision_object<T> &comparingObjects){
        if(planet1 == comparingObjects.getObj(1)){
            return true;
        }
        if(planet1 == comparingObjects.getObj(2)){
            return true;
        }
        if(planet2 == comparingObjects.getObj(1)){
            return true;
        }
        if(planet2 == comparingObjects.getObj(2)){
            return true;
        }
        return false;
    }

    GMlib::Vector<T,3> getNewVelocity(int i) const{
        if(i == 1)
            return newVelocityPlanet1;
        if(i == 2)
            return newVelocityPlanet2;
        else
            return GMlib::Vector<T,3>();
    }

    void setVelocity(){

        T planet1Mass = planet1->getMass();
        T planet2Mass = planet2->getMass();

        GMlib::Vector<T,3> velocityPlanet1 = planet1->getVelocity();
        GMlib::Vector<T,3> velocityPlanet2 = planet2->getVelocity();

        newVelocityPlanet1 = (velocityPlanet1*(planet1Mass-planet2Mass)+2*planet2Mass*velocityPlanet2)/(planet1Mass+planet2Mass);
        newVelocityPlanet2 = (velocityPlanet2*(planet2Mass-planet1Mass)+2*planet1Mass*velocityPlanet1)/(planet1Mass+planet2Mass);

        //shadowmapping
        //atmosfær scattering
    }



protected:



private:

    std::shared_ptr<Sphere<T>>  planet1;
    std::shared_ptr<Sphere<T>>  planet2;
    GMlib::Vector<T,3> newVelocityPlanet1;
    GMlib::Vector<T,3> newVelocityPlanet2;

    T x;

}; // END class Sphere


#endif // COLLISION_OBJECT

