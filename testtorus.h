#ifndef TESTTORUS_H
#define TESTTORUS_H


#include <parametrics/gmptorus>


class TestTorus : public GMlib::PTorus<float> {
public:
  using PTorus::PTorus;

  ~TestTorus() {

    if(m_test01)
      remove(test_01_torus.get());
    if(m_test02)
      remove(test_02_torus.get());
    if(m_test03)
      remove(test_03_torus.get());
    if(m_test04)
      remove(test_04_torus.get());
    }


  void test01() {

    GMlib::Vector<float,3> d1 = evaluate(0.0f,0.0f,0,0)[0][0];
    test_01_torus = std::make_shared<TestTorus,float,float,float>(1.5f,0.5f,0.5f);
    test_01_torus->translate(d1 + d1.getNormalized()*12.0f);
    test_01_torus->rotate( GMlib::Angle(0), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f) );
    test_01_torus->toggleDefaultVisualizer();
    test_01_torus->setMaterial(GMlib::GMmaterial::Copper);
    test_01_torus->replot(200,200,1,1);
    insert(test_01_torus.get());


    test_02_torus = std::make_shared<TestTorus,float,float,float>(1.5f,0.5f,0.5f);
    test_02_torus->translate(d1 - d1.getNormalized()*20.0f);
    test_02_torus->rotate( GMlib::Angle(90), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f) );
    test_02_torus->toggleDefaultVisualizer();
    test_02_torus->setMaterial(GMlib::GMmaterial::Silver);
    test_02_torus->replot(200,200,1,1);
    insert(test_02_torus.get());


    GMlib::Vector<float,3> d2 = evaluate(0.0f,0.0f,0,0)[1][0];
    test_03_torus = std::make_shared<TestTorus,float,float,float>(1.5f,0.5f,0.5f);
    test_03_torus->translate(d2 - d2.getNormalized()*30.0f);
    test_03_torus->rotate( GMlib::Angle(270), GMlib::Vector<float,3>( 0.0f,0.0f, 1.0f) );
    test_03_torus->toggleDefaultVisualizer();
    test_03_torus->setMaterial(GMlib::GMmaterial::Bronze);
    test_03_torus->replot(200,200,1,1);
    insert(test_03_torus.get());


    test_04_torus = std::make_shared<TestTorus,float,float,float>(1.5f,0.5f,0.5f);
    test_04_torus->translate(d2 + d2.getNormalized()*22.0f);
    test_04_torus->rotate( GMlib::Angle(360), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f) );
    test_04_torus->toggleDefaultVisualizer();
    test_04_torus->setMaterial(GMlib::GMmaterial::Gold);
    test_04_torus->replot(200,200,1,1);
    insert(test_04_torus.get());


    //test_01_torus->insert(test_04_torus.get());

    //test_04_torus->lock(test_03_torus.get());
    //test_03_torus->lock(test_04_torus.get());
    //test_03_torus->lock(test_01_torus.get());
    //test_02_torus->lock(test_01_torus.get());

    m_test01 = true;
    m_test02 = true;
    m_test03 = true;
    m_test04 = true;
  }


protected:
  void localSimulate(double dt) override {


      //translate (GMlib::Vector<float,3>( 0.1f, 0.1f, 0.1f ) );
      rotate( GMlib::Angle(90) * dt, GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f ) );
  }

private:
  bool m_test01 {false};
  bool m_test02 {false};
  bool m_test03 {false};
  bool m_test04 {false};
  std::shared_ptr<TestTorus> test_01_torus {nullptr};
  std::shared_ptr<TestTorus> test_02_torus {nullptr};
  std::shared_ptr<TestTorus> test_03_torus {nullptr};
  std::shared_ptr<TestTorus> test_04_torus {nullptr};

}; // END class TestTorus



#endif // TESTTORUS_H
