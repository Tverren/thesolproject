/* \file TestSurfaceNineVector.cpp
 *
 *  Implementation of the TestSurfaceNineVector template class.
 */


// for syntaxhighlighting
#include "testsnv.h"

namespace GMlib {


template <typename T>
inline
TestSurfaceNineVector<T>::TestSurfaceNineVector(const DMatrix<Vector<T,3>>& c) {

    _c  = c;

    this->_dm = GM_DERIVATION_EXPLICIT;
}

template <typename T>
inline
TestSurfaceNineVector<T>::TestSurfaceNineVector( const TestSurfaceNineVector<T>& copy ) : PSurf<T,3>( copy ) {

    _c  = copy._c;
}


template <typename T>
TestSurfaceNineVector<T>::~TestSurfaceNineVector() {}

template <typename T>
void TestSurfaceNineVector<T>::eval(T u, T v, int d1, int d2, bool /*lu*/, bool /*lv*/ ) {

    this->_p.setDim( d1+1, d2+1 );


    //((1-u)^2, 2u(1-u), u^2)
    DVector<T> u1(3);
    u1[0] = (1-u)*(1-u);
    u1[1] = 2*u*(1-u);
    u1[2] = u*u;

    // (1-v)^2
    //  2v(1-v)
    //  v^2
    DVector<T> v1(3);
    v1[0] = (1-v)*(1-v);
    v1[1] = 2*v*(1-v);
    v1[2] = v*v;

    this->_p[0][0] = u1*(_c^v1);

    if( this->_dm == GM_DERIVATION_EXPLICIT ) {

        // 1st
        DVector<T> u2(3);
        u2[0] = -2*(1-u);
        u2[1] = 2-4*u;
        u2[2] = 2*u;
        if(d1){
            this->_p[1][0] = u2*(_c^v1); // S_u
        }

        DVector<T> v2(3);
        v2[0] = -2*(1-v);
        v2[1] = 2-4*v;
        v2[2] = 2*v;
        if(d2){
            this->_p[0][1] = u1*(_c^v2); // S_v
        }

        if(d1>1 && d2>1)  this->_p[1][1] = u2*(_c^v2); // S_uv

        // 2nd
        if(d1>1)          this->_p[2][0] = Vector<T,3>(T(0)); // S_uu
        if(d2>1)          this->_p[0][2] = Vector<T,3>(T(0)); // S_vv
        if(d1>1 && d2)    this->_p[2][1] = Vector<T,3>(T(0)); // S_uuv
        if(d1   && d2>1)  this->_p[1][2] = Vector<T,3>(T(0)); // S_uvv
        if(d1>1 && d2>1)  this->_p[2][2] = Vector<T,3>(T(0)); // S_uuvv

        // 3rd
        if(d1>2)          this->_p[3][0] = Vector<T,3>(T(0)); // S_uuu
        if(d2>2)          this->_p[0][3] = Vector<T,3>(T(0)); // S_vvv
        if(d1>2 && d2)    this->_p[3][1] = Vector<T,3>(T(0)); // S_uuuv
        if(d1   && d2>2)  this->_p[1][3] = Vector<T,3>(T(0)); // S_uvvv
        if(d1>2 && d2>1)  this->_p[3][2] = Vector<T,3>(T(0)); // S_uuuvv
        if(d1>1 && d2>2)  this->_p[2][3] = Vector<T,3>(T(0)); // S_uuvvv
        if(d1>2 && d2>2)  this->_p[3][3] = Vector<T,3>(T(0)); // S_uuuvvv
    }
}


template <typename T>
inline
T TestSurfaceNineVector<T>::getEndPU()	{

    return T(1);
}

template <typename T>
inline
T TestSurfaceNineVector<T>::getEndPV()	{

    return T(1);
}


template <typename T>
inline
T TestSurfaceNineVector<T>::getStartPU() {

    return T(0);
}


template <typename T>
inline
T TestSurfaceNineVector<T>::getStartPV() {

    return T(0);
}
}
