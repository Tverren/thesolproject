
/* \file testsurface.h
 *
 *  Interface for the testSurface class.
 */

#ifndef __testSurface_H__
#define __testSurface_H__

#include "../gmlib/modules/parametrics/src/gmpsurf.h"


namespace GMlib {

template <typename T>
class TestSurface : public PSurf<T,3> {
    GM_SCENEOBJECT(TestSurface)
    public:
        TestSurface(  const Point<T,3>& p1,
                      const Point<T,3>& p2,
                      const Point<T,3>& p3,
                      const Point<T,3>& p4 );
    TestSurface( const TestSurface<T>& copy );
    virtual ~TestSurface();

protected:
    Point<T,3>		            _pt1;
    Point<T,3>		            _pt2;
    Point<T,3>		            _pt3;
    Point<T,3>		            _pt4;

    void                      eval(T u, T v, int d1, int d2, bool lu = true, bool lv = true );
    T                         getEndPU();
    T                         getEndPV();
    T                         getStartPU();
    T                         getStartPV();

}; // END class testSurface

} // END namespace GMlib

// Include testSurface class function implementations
#include "testsurface.cpp"



#endif // __testsurface_H__
