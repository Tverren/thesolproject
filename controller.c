#include <gmParametricsModule>
#include "controller.h"
#include <random>



/*
 *

template<typename T>
void Controller<T>::addSphere(){

    //creates sun with,                       size, mass, density and vector speed
    auto sun = std::make_shared<Sphere<T>>(sunSize, sunMass, sunDensity, GMlib::Vector<T,3>(0.0f, 0.0f, 0.0f));
    sun->translate(GMlib::Vector<float,3>(sunPosition,sunPosition,sunPosition));//spawn location
    sun->toggleDefaultVisualizer();//
    //sun->setMaterial(GMlib::GMmaterial::Gold);
    sun->setMaterial(GMlib::GMcolor::Yellow);
    sun->replot(200,200,1,1);//
    planetOfSphere.push_back(sun);//puts planet in array
    this->insert(sun.get());//inserts planet in controller


    //creates mercury with,                        size, mass, density and vector speed
    auto mercury = std::make_shared<Sphere<T>>(mercurySize, mercuryMass, mercuryDensity, GMlib::Vector<T,3>(0.0f, mercuryVelocity, 0.0f));
    mercury->translate(GMlib::Vector<float,3>(mercuryPosition,float(0.0),float(0.0)));//spawn location
    mercury->toggleDefaultVisualizer();//
    //sun->setMaterial(GMlib::GMmaterial::Gold);
    mercury->setMaterial(GMlib::GMcolor::Grey);
    mercury->replot(200,200,1,1);//
    planetOfSphere.push_back(mercury);//puts planet in array
    this->insert(mercury.get());//inserts planet in controller


    //creates planet with,                size, mass, density and vector speed
    auto venus = std::make_shared<Sphere<T>>(venusSize, venusMass, venusDensity, GMlib::Vector<T,3>(0.0f, venusVelocity, 0.0f));
    venus->translate(GMlib::Vector<float,3>(float(venusPosition),float(0.0),float(0.0)));//spawn location
    venus->toggleDefaultVisualizer();//
    venus->setMaterial(GMlib::GMcolor::Beige);
    venus->replot(200,200,1,1);//
    planetOfSphere.push_back(venus);//puts planet in array
    this->insert(venus.get());//inserts planet in controller


    //creates planet with,                size, mass, density and vector speed
    auto earth = std::make_shared<Sphere<T>>(earthSize, earthMass, earthDensity, GMlib::Vector<T,3>(0.0f, earthVelocity, 0.0f));
    earth->translate(GMlib::Vector<float,3>(float(earthPosition),float(0.0),float(0.0)));//spawn location
    earth->toggleDefaultVisualizer();//
    //earth->setMaterial(GMlib::GMmaterial::Emerald);
    earth->setMaterial(GMlib::GMcolor::Green);
    earth->replot(200,200,1,1);//
    planetOfSphere.push_back(earth);//puts planet in array
    this->insert(earth.get());//inserts planet in controller

    //creates planet with,                size, mass, density and vector speed
    auto mars = std::make_shared<Sphere<T>>(marsSize, marsMass, marsDensity, GMlib::Vector<T,3>(0.0f, marsVelocity, 0.0f));
    mars->translate(GMlib::Vector<float,3>(float(marsPosition),float(0.0),float(0.0)));//spawn location
    mars->toggleDefaultVisualizer();//
    //mars->setMaterial(GMlib::GMmaterial::Ruby);
    mars->setMaterial(GMlib::GMcolor::Red);
    mars->replot(200,200,1,1);//
    planetOfSphere.push_back(mars);//puts planet in array
    this->insert(mars.get());//inserts planet in controller

/*
    std::default_random_engine generator;
    std::uniform_real_distribution<double> distributionDistance(0,10000);

    T marsjup = ((778299-227939)*sunSize)/sizeScaling;
    T solMars = (227939*sunSize)/sizeScaling;

  //std::uniform_real_distribution<double> distributionSpeed(13070,24077);
    std::uniform_real_distribution<double> distributionSpeed(25000,26000);

  //std::uniform_real_distribution<double> distributionMass(1e12, 1e18);
    std::uniform_real_distribution<double> distributionMass(6.4171e18, 1.8986e21);

    int numberOfAsteroids = 100;
    for(int i = 0; i < numberOfAsteroids; i++){

        a = (i/numberOfAsteroids)*360;
         *
         * dX =rand()
         * vY = rand()%
         * v = <0,vy,0>
         * d = <dX, 0,0>
         * vN = rotate(v,a)
         * dN = rotate(d,a)
         * b->translate(dN)
         * b-velocity(vN)

        T size = earthSize*0.100;
        T mass = distributionMass(generator);

        T density = rand()%300+50;

        T dis = distributionDistance(generator)/10000;
        T newMarsJup = (marsjup*dis)+solMars;


        //T dX = distributionDistance(generator);
        //T dX_scale = (dX*sunSize)/sizeScaling;

        T dX = newMarsJup;

        T vector_speed_y = distributionSpeed(generator);
        T vY_scale = vector_speed_y*universal_scaling;


        //creates planet with, size, mass, density and vector speed
        auto asteroids = std::make_shared<Sphere<T>>(size, mass*universal_scaling, density, GMlib::Vector<T,3>(0.0f,vY_scale,0.0f));
        asteroids->translate(GMlib::Vector<float,3>(dX,0.0f,0.0f));//spawn location

        asteroids->toggleDefaultVisualizer();//
        asteroids->setMaterial(GMlib::GMmaterial::Snow);
        asteroids->replot(200,200,1,1);//
        this->insert(asteroids.get());//inserts planet in controller
        planetOfSphere.push_back(asteroids);//puts planet in array
    }

    //creates planet with,                size, mass, density and vector speed
    auto jupiter = std::make_shared<Sphere<T>>(jupiterSize, jupiterMass, jupiterDensity, GMlib::Vector<T,3>(0.0f, jupiterVelocity, 0.0f));
    jupiter->translate(GMlib::Vector<float,3>(float(jupiterPosition),float(0.0),float(0.0)));//spawn location
    jupiter->toggleDefaultVisualizer();//
    //jupiter->setMaterial(GMlib::GMmaterial::Brass);
    jupiter->setMaterial(GMlib::GMcolor::AntiqueWhite);
    jupiter->replot(200,200,1,1);//
    planetOfSphere.push_back(jupiter);//puts planet in array
    this->insert(jupiter.get());//inserts planet in controller


    //creates planet with,                size, mass, density and vector speed
    auto saturn = std::make_shared<Sphere<T>>(saturnSize, saturnMass, saturnDensity, GMlib::Vector<T,3>(0.0f, saturnVelocity, 0.0f));
    saturn->translate(GMlib::Vector<float,3>(float(saturnPosition),float(0.0),float(0.0)));//spawn location
    saturn->toggleDefaultVisualizer();//
    //saturn->setMaterial(GMlib::GMmaterial::PolishedGold);
    saturn->setMaterial(GMlib::GMcolor::LightYellow);
    saturn->replot(200,200,1,1);//
    planetOfSphere.push_back(saturn);//puts planet in array
    this->insert(saturn.get());//inserts planet in controller


    //creates planet with,                size, mass, density and vector speed
    auto uranus = std::make_shared<Sphere<T>>(uranusSize, uranusMass, uranusDensity, GMlib::Vector<T,3>(0.0f, uranusVelocity, 0.0f));
    uranus->translate(GMlib::Vector<float,3>(float(uranusPosition),float(0.0),float(0.0)));//spawn location
    uranus->toggleDefaultVisualizer();//
    //uranus->setMaterial(GMlib::GMmaterial::Pearl);
    uranus->setMaterial(GMlib::GMcolor::LightBlue);
    uranus->replot(200,200,1,1);//
    planetOfSphere.push_back(uranus);//puts planet in array
    this->insert(uranus.get());//inserts planet in controller


    //creates planet with,                size, mass, density and vector speed
    auto neptune = std::make_shared<Sphere<T>>(neptuneSize, neptuneMass, neptuneDensity, GMlib::Vector<T,3>(0.0f, neptuneVelocity, 0.0f));
    neptune->translate(GMlib::Vector<float,3>(float(neptunePosition),float(0.0),float(0.0)));//spawn location
    neptune->toggleDefaultVisualizer();//
    //neptune->setMaterial(GMlib::GMmaterial::Sapphire);
    neptune->setMaterial(GMlib::GMcolor::DarkBlue);
    neptune->replot(200,200,1,1);//
    planetOfSphere.push_back(neptune);//puts planet in array
    this->insert(neptune.get());//inserts planet in controller


}
 */

/*
 *Adds two spheres on collision course horisontal
 */
template<typename T>
void Controller<T>::addTwoSphereCollHor(){

    //creates sun with,                       size, mass, density and vector speed
    auto planet1 = std::make_shared<Sphere<T>>(50, 600, 0, GMlib::Vector<T,3>(0.0f, 0.0f, 0.0f));
    planet1->translate(GMlib::Vector<float,3>(float(-100.0),float(0.0),float(0.0)));//spawn location
    planet1->toggleDefaultVisualizer();//
    //sun->setMaterial(GMlib::GMmaterial::Gold);
    planet1->setMaterial(GMlib::GMcolor::Yellow);
    planet1->replot(200,200,1,1);//
    planetOfSphere.push_back(planet1);//puts planet in array
    this->insert(planet1.get());//inserts planet in controller


    //creates sun with,                        size, mass, density and vector speed
    auto planet2 = std::make_shared<Sphere<T>>( 20, 10, 0, GMlib::Vector<T,3>(0.0f, 0.0f, 0.0f));
    planet2->translate(GMlib::Vector<float,3>(float(100.0),float(0.0),float(0.0)));//spawn location
    planet2->toggleDefaultVisualizer();//
    //sun->setMaterial(GMlib::GMmaterial::Gold);
    planet2->setMaterial(GMlib::GMcolor::Red);
    planet2->replot(200,200,1,1);//
    planetOfSphere.push_back(planet2);//puts planet in array
    this->insert(planet2.get());//inserts planet in controller

}
/*
 *Adds two spheres on a 45 degree collision course
 */
template<typename T>
void Controller<T>::addTwoSphereCollVer(){

    //creates sun with,                       size, mass, density and vector speed
    auto planet1 = std::make_shared<Sphere<T>>(50, 600, 0, GMlib::Vector<T,3>(0.0f, 0.0f, 0.0f));
    planet1->translate(GMlib::Vector<float,3>(float(-100.0),float(0.0),float(0.0)));//spawn location
    planet1->toggleDefaultVisualizer();//
    //sun->setMaterial(GMlib::GMmaterial::Gold);
    planet1->setMaterial(GMlib::GMcolor::Yellow);
    planet1->replot(200,200,1,1);//
    planetOfSphere.push_back(planet1);//puts planet in array
    this->insert(planet1.get());//inserts planet in controller


    //creates sun with,                        size, mass, density and vector speed
    auto planet2 = std::make_shared<Sphere<T>>( 20, 10, 0, GMlib::Vector<T,3>(0.0f, 0.0f, 0.0f));
    planet2->translate(GMlib::Vector<float,3>(float(100.0),float(100.0),float(0.0)));//spawn location
    planet2->toggleDefaultVisualizer();//
    //sun->setMaterial(GMlib::GMmaterial::Gold);
    planet2->setMaterial(GMlib::GMcolor::Red);
    planet2->replot(200,200,1,1);//
    planetOfSphere.push_back(planet2);//puts planet in array
    this->insert(planet2.get());//inserts planet in controller

}

/*
 *Adds two spheres where one is in orbit of another
 */
template<typename T>
void Controller<T>::addTwoSphereOrb(){

    //creates sun with,                       size, mass, density and vector speed
    auto planet1 = std::make_shared<Sphere<T>>(100, 100000000, 10, GMlib::Vector<T,3>(0.0f, 0.0f, 0.0f));
    planet1->translate(GMlib::Vector<float,3>(float(0.0),float(0.0),float(0.0)));//spawn location
    planet1->toggleDefaultVisualizer();//
    //sun->setMaterial(GMlib::GMmaterial::Gold);
    planet1->setMaterial(GMlib::GMcolor::Yellow);
    planet1->replot(200,200,1,1);//
    planetOfSphere.push_back(planet1);//puts planet in array
    this->insert(planet1.get());//inserts planet in controller


    //creates sun with,                        size, mass, density and vector speed
    auto planet2 = std::make_shared<Sphere<T>>(10, 10000, 10, GMlib::Vector<T,3>(0.0f, 0.003f, 0.0f));
    planet2->translate(GMlib::Vector<float,3>(float(500.0),float(0.0),float(0.0)));//spawn location
    planet2->toggleDefaultVisualizer();//
    //sun->setMaterial(GMlib::GMmaterial::Gold);
    planet2->setMaterial(GMlib::GMcolor::Red);
    planet2->replot(200,200,1,1);//
    planetOfSphere.push_back(planet2);//puts planet in array
    this->insert(planet2.get());//inserts planet in controller
}

/*
 *Adds three spheres where two is in orbit of one
 */
template<typename T>
void Controller<T>::addThreeSphereOrb(){

        /*
    //creates sun with,                       size, mass, density and vector speed
    auto sun = std::make_shared<Sphere<T>>(sunSize, 1.98855e30*universal_scaling, 300, GMlib::Vector<T,3>(0.0f, 0.0f, 0.0f));
    sun->translate(GMlib::Vector<float,3>(float(0.0),float(0.0),float(0.0)));//spawn location
    sun->toggleDefaultVisualizer();//
    //sun->setMaterial(GMlib::GMmaterial::Gold);
    sun->setMaterial(GMlib::GMcolor::Yellow);
    sun->replot(200,200,1,1);//
    planetOfSphere.push_back(sun);//puts planet in array
    this->insert(sun.get());//inserts planet in controller



    //creates mercury with,                        size, mass, density and vector speed
    auto mercury = std::make_shared<Sphere<T>>(earthSize*0.3829f, 3.3011e23*universal_scaling, 300, GMlib::Vector<T,3>(0.0f, 47362.0f*universal_scaling, 0.0f));
    mercury->translate(GMlib::Vector<float,3>(float((57909.05f*sunSize)/sizeScaling),float(0.0),float(0.0)));//spawn location
    mercury->toggleDefaultVisualizer();//
    //sun->setMaterial(GMlib::GMmaterial::Gold);
    mercury->setMaterial(GMlib::GMcolor::Red);
    mercury->replot(200,200,1,1);//
    planetOfSphere.push_back(mercury);//puts planet in array
    this->insert(mercury.get());//inserts planet in controller
    */


    //creates sun with,                       size, mass, density and vector speed
    auto planet = std::make_shared<Sphere<T>>(100, 1e15, 10, GMlib::Vector<T,3>(0.0f, 0.0f, 0.0f));
    planet->translate(GMlib::Vector<float,3>(float(0.0), float(0.0), float(0.0)));//spawn location
    planet->toggleDefaultVisualizer();//
    //sun->setMaterial(GMlib::GMmaterial::Gold);
    planet->setMaterial(GMlib::GMcolor::Green);
    planet->replot(200,200,1,1);//
    planetOfSphere.push_back(planet);//puts planet in array
    this->insert(planet.get());//inserts planet in controller



    //creates mercury with,                        size, mass, density and vector speed
    auto moon = std::make_shared<Sphere<T>>(10, 1e10, 10, GMlib::Vector<T,3>(0.0f, 7.30000f, 0.0f));
    moon->translate(GMlib::Vector<float,3>(float(1000),float(0.0),float(0.0)));//spawn location
    moon->toggleDefaultVisualizer();//
    //sun->setMaterial(GMlib::GMmaterial::Gold);
    moon->setMaterial(GMlib::GMcolor::Red);
    moon->replot(200,200,1,1);//
    planetOfSphere.push_back(moon);//puts planet in array
    this->insert(moon.get());//inserts planet in controller



/*
    //creates venus with,                        size, mass, density and vector speed
    auto venus = std::make_shared<Sphere<T>>(earthSize*0.9499f, 4.8675e24*universal_scaling, 300, GMlib::Vector<T,3>(0.0f, 35020.0f*universal_scaling, 0.0f));
    venus->translate(GMlib::Vector<float,3>(float((108208.0*sunSize)/sizeScaling),float(0.0),float(0.0)));//spawn location
    venus->toggleDefaultVisualizer();//
    //sun->setMaterial(GMlib::GMmaterial::Gold);
    venus->setMaterial(GMlib::GMcolor::Red);
    venus->replot(200,200,1,1);//
    planetOfSphere.push_back(venus);//puts planet in array
    this->insert(venus.get());//inserts planet in controller
*/
}


/*
 *

template<typename T>
GMlib::Vector<T,3> Controller<T>::compute_force(std::shared_ptr<Sphere<T>> planet1,
                                                std::shared_ptr<Sphere<T>> planet2){
    auto pgp1 = planet1->getGlobalPos();
    auto pgp2 = planet2->getGlobalPos();

    GMlib::Point<T,3> planet1_position(pgp1(0), pgp1(1),pgp1(2));
    GMlib::Point<T,3> planet2_position(pgp2(0), pgp2(1),pgp2(2));

    //std::cout << "planet1_position " << planet1_position << std::endl;
    //std::cout << "planet2_position " << planet2_position << std::endl;

    auto pgp1_2 = (planet1_position-planet2_position);
    //this calculate the force between the objects
    auto force_of_gravity = universal_gravity*(planet1->getMass()*planet2->getMass())/((pgp1_2)*(pgp1_2));


    //std::cout << "force_of_gravity  " << force_of_gravity << std::endl;


    GMlib::Vector<T,3> force_direction = pgp1_2/std::sqrt(pgp1_2*pgp1_2);

    auto f = force_direction*force_of_gravity*universal_scaling;

    //std::cout << "f  " << f.getLength() << std::endl;

    return f;
}
 */


/*
 *taylor rekke

template<typename T>
void Controller<T>::compute_step(T dt){

    for(int i = 0; i < planetOfSphere.size(); i++){

        GMlib::Vector<T,3> sphereForce = planetOfSphere[i]->getForce();
        T sphereMass =  planetOfSphere[i]->getMass();

        GMlib::Vector<T,3> force_derived =        (planetOfSphere[i]->getForcePrevious() - sphereForce)/dt;
        GMlib::Vector<T,3> force_derived_double = (planetOfSphere[i]->getForcePreviousPrevious() - sphereForce)/(dt+dtPrev);
        GMlib::Vector<T,3> force_derived_triple = (planetOfSphere[i]->getForcePreviousPreviousPrevious() - sphereForce)/(dt+dtPrevPrev+dtPrev);

        //std::cout << "force_derived             controller.c   " << force_derived<< std::endl;
        //std::cout << "force_derived_double      controller.c   " << force_derived_double<< std::endl;
        //std::cout << "force_derived_triple      controller.c   " << force_derived_triple<< std::endl;

        auto ds = (dt*planetOfSphere[i]->getVelocity())
                   + (((1/2)*  (dt*dt)*         sphereForce)/sphereMass)
                   + (((1/6)*  (dt*dt*dt)*      force_derived)/sphereMass)
                   + (((1/24)* (dt*dt*dt*dt)*   force_derived_double)/sphereMass)
                   + (((1/120)*(dt*dt*dt*dt*dt)*force_derived_triple)/sphereMass);

        //std::cout << "ds             controller.c   " << ds<< std::endl;


        planetOfSphere[i]->update_velocity   (dt*((sphereForce/sphereMass)
                                              + (1/2)* (dt*dt)*      (force_derived/sphereMass)
                                              + (1/6)* (dt*dt*dt)*   (force_derived_double/sphereMass)
                                              + (1/24)*(dt*dt*dt*dt)*(force_derived_triple/sphereMass)));


        //std::cout << "ds             controller.c   " << ds<< std::endl;

        planetOfSphere[i]->reset_force();
        planetOfSphere[i]->update_step(ds);

        dtPrevPrev = dtPrev;
        dtPrev = dt;
        //std::cout << " this is the ds: " << ds.getLength() << std::endl;
        //std::cout << " this is the dt: " << dt << std::endl;
    }
}


/*
 *
 */


/*
 *

template<typename T>
void Controller<T>::make_force(T dt){
    for (int i = 0; i < planetOfSphere.size() - 1; i++){
        for (int j = 1+i; j < planetOfSphere.size(); j++){
            GMlib::Vector<T,3> f = compute_force(planetOfSphere[i],
                                                 planetOfSphere[j]);

            //std::cout << "f:   " << f.getLength() << std::endl;

            planetOfSphere[i]->update_force(-f);
            planetOfSphere[j]->update_force(f);
        }
    }
    for(int i = 0; i < planetOfSphere.size(); i++){
        planetOfSphere[i]->compute_step(dt);
    }
}
 */

/*
 *
 */
template<typename T>
void Controller<T>::compute_force(T dt){

    for (int i = 0; i < planetOfSphere.size() - 1; i++){
        for (int j = i+1; j < planetOfSphere.size(); j++){

            auto pgpI = planetOfSphere[i]->getGlobalPos();
            auto pgpJ = planetOfSphere[j]->getGlobalPos();


            GMlib::Point<T,3> planetI_position(pgpI(0), pgpI(1), pgpI(2));
            GMlib::Point<T,3> planetJ_position(pgpJ(0), pgpJ(1), pgpJ(2));

            GMlib::Vector<T,3> pgp_IJ = (planetI_position - planetJ_position);

            auto force_of_gravity = universal_gravity*(planetOfSphere[i]->getMass()*planetOfSphere[j]->getMass())/(pgp_IJ*pgp_IJ);

            GMlib::Vector<T,3> force_direction = pgp_IJ/pgp_IJ.getLength();

            auto force = force_direction*force_of_gravity;
            //auto force = force_direction*force_of_gravity*universal_scaling;

            planetOfSphere[i]->update_force(-force);
            planetOfSphere[j]->update_force(force);
        }
    }
    for(int i = 0; i < planetOfSphere.size(); i++){
        planetOfSphere[i]->compute_step(dt);
    }
}


/*
 *
 */
template<typename T>
void Controller<T>::collision_detection(T dt){

    std::vector<Collision_object<T>> coll;
    for (int i = 0; i < planetOfSphere.size(); i++){
        for (int j = 1+i; j < planetOfSphere.size(); j++){
            Collision_object<T> object;
            if(find_collision(planetOfSphere[i], planetOfSphere[j], dt, 0, object)){
                coll.push_back(object);
            }
        }
    }
    while(coll.size()>0){
        std::sort(coll.begin(), coll.end());
        std::unique(coll.begin(), coll.end());
        Collision_object<T> cob = coll[0];
        coll.erase(coll.begin());

        //std::cout << " this is inside while wowowoowowowoowowowo" << std::endl;

        cob.getObj(1)->update_sphere(cob.getNewVelocity(1));
        cob.getObj(2)->update_sphere(cob.getNewVelocity(2));

        for (int i = 0; i < planetOfSphere.size(); i++){
            Collision_object<T> object;
            if(cob.getObj(1) != planetOfSphere[i] && cob.getObj(2) != planetOfSphere[i]){
                if(find_collision(cob.getObj(2), planetOfSphere[i], dt, cob.getX(), object))
                    coll.push_back(object);
                if(find_collision(cob.getObj(1), planetOfSphere[i], dt, cob.getX(), object))
                    coll.push_back(object);
            }
        }
    }
}

/*
 *
 */
template<typename T>
bool Controller<T>::find_collision(std::shared_ptr<Sphere<T>> pi ,std::shared_ptr<Sphere<T>> pj,
                                   T dt, T newX, Collision_object<T> &object){
    auto cpgp1 = pi->getGlobalPos();
    auto cpgp2 = pj->getGlobalPos();

    GMlib::Point<T,3> pi_getGlobalPos(cpgp1(0), cpgp1(1), cpgp1(2));
    GMlib::Point<T,3> pj_getGlobalPos(cpgp2(0), cpgp2(1), cpgp2(2));

    GMlib::Vector<T,3> dp = (pi_getGlobalPos - pj_getGlobalPos);
    GMlib::Vector<T,3> ds = (pi->getStep() - pj->getStep());
    T planets_radius = (pi->getRadius() + pj->getRadius());

    T a = ds * ds;
    T b = 2 * (ds * dp);
    T c = (dp * dp) - (planets_radius * planets_radius);
    T var = (b*b)-4*a*c;

    if(var > 0){
        newX = (-b - std::sqrt(var))/(2*a);
        if(newX < 1 && newX > 0){
            object.setX(newX);
            object.setObj(1,pi);
            object.setObj(2,pj);
            object.setVelocity();
            //std::cout << newX << " this is x wowowoowowoowowowowoowowo" << std::endl;
            return true;
        }
    }
    return false;
}

template<typename T>
Controller<T>::~Controller(){
    planetOfSphere.clear();
}

